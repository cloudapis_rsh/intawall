/**
 * Created by Hans Van Staey on 25/04/2016.
 */

var express = require("express");
var bodyparser = require("body-parser");
var request = require("request");
var app = express();
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({
    extended: true
}));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header ('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    next();
});

// Set your secret key: remember to change this to your live secret key in production
// See your keys here https://dashboard.stripe.com/account/apikeys
var stripe = require("stripe")("sk_test_JoA0Vz97U1dMiGfYP89I69to");


var path = require('path');
app.get("/",function(req,res){
    res.sendFile(__dirname + "/" + "index.html");
});
app.get("/controller.js",function(req,res){
    res.sendFile(__dirname + "/" + "controller.js")
});


app.post('/payOnce', function (req, res) {
    console.log("pay once executing");

    // Get the credit card details submitted by the form
    var stripeToken = req.body.stripeToken;
    
    var charge = stripe.charges.create({
        amount: 1000, // amount in cents, again
        currency: "eur",
        source: stripeToken,
        description: "Example charge"
    }, function(err, charge) {
        if (err && err.type === 'StripeCardError') {
            // The card has been declined
        } else {
            res.send(charge);
        }
    });
    console.log(req.body);
});

app.post('/pay', function (req, res) {

    console.log("entered pay, pay more than once XD");

    var stripeToken = req.body.form.stripeToken;
    var price = req.body.price;

    stripe.customers.create({
        source: stripeToken,
        description: 'payinguser@example.com'
    }).then(function (customer) {
        return stripe.charges.create({
            amount: price, // amount in cents, again
            currency: "eur",
            customer: customer.id
        });
    }).then(function (charge) {
        if( !charge.failure_code && !charge.failure_message){
            var object = {
                msg: "Success: Payment was successful",
                data: charge,
                status: 200,
                success: true,
                error: charge.failure_message
            };
            console.log("Post response: PAY: payment success");
            res.send(object);
        }else{
            var object = {
                msg: "Error: Payment was unsuccessful",
                data: charge,
                status: 300,
                success: false,
                error:charge.failure_message
            };
            console.log("Post response: PAY: payment fail");
            res.send(object);
        }
    });

});

app.listen(3000);