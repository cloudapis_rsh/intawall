/**
 * Created by rival on 6/03/2016.
 */


var express = require("express");
var bodyparser = require("body-parser");
var request = require("request");
var Firebase = require("firebase");
var app = express();
var stripe = require("stripe")(process.env.stripe_id);
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({
    extended: true
}));

var mongoose = require('mongoose');

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header ('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    next();
});

app.get("/",function(req,res){
    res.sendFile(__dirname + "/" + "index.html");
});
app.get("/css/reset.css",function(req,res){
    res.sendFile(__dirname + "/css/" + "reset.css");
});
app.get("/css/animation.css",function(req,res){
    res.sendFile(__dirname + "/css/" + "animation.css");
});
app.get("/js/homeController.js",function(req,res){
    res.sendFile(__dirname + "/js/" + "homeController.js");
});
app.get("/js/LoginCtrl.js",function(req,res){
    res.sendFile(__dirname + "/js/" + "LoginCtrl.js");
});

app.get("/js/paymentController.js",function (req,res) {
   res.sendFile(__dirname + "/js/paymentController.js") ;
});

app.get("/js/Services.js",function (req,res) {
    res.sendFile(__dirname + "/js/Services.js") ;
});

app.post('/pay', function (req, res) {

    console.log("entered pay, pay more than once XD");

    var stripeToken = req.body.form.stripeToken;
    var price = req.body.price;

    stripe.customers.create({
        source: stripeToken,
        description: req.body.username
    }).then(function (customer) {
        return stripe.charges.create({
            amount: price, // amount in cents, again
            currency: "eur",
            customer: customer.id
        });
    }).then(function (charge) {
        if( !charge.failure_code && !charge.failure_message){
            var object = {
                msg: "Success: Payment was successful",
                data: charge,
                status: 200,
                success: true,
                error: charge.failure_message
            };
            console.log("Post response: PAY: payment success");
            res.send(object);
        }else{
            var object = {
                msg: "Error: Payment was unsuccessful",
                data: charge,
                status: 300,
                success: false,
                error:charge.failure_message
            };
            console.log("Post response: PAY: payment fail");
            res.send(object);
        }
    });

});


app.delete('/api/activescreens', function (req,res) {
    var instagramName = req.body.userName;

    var instagramUserObjs = req.body.userObject;

    if(!!instagramName){
        instagramName = FireBase_Child_Check(instagramName);
        FireBase_Reset_ActiveScreens(instagramName,function (awnser1) {
            res.send(awnser1);
        });

    }else if(!!instagramUserObjs && !!instagramUserObjs.username){
        instagramUserObjs.username = FireBase_Child_Check( instagramUserObjs.username);
        FireBase_Reset_ActiveScreens(instagramUserObjs.username,function (awnser2) {
            res.send(awnser2);
        });
    }else{
        var object = {
            msg:"Wrong api Call, needs one of the expected object",
            status:401,
            data:{
                expectedobject: {userName:"instagramUsername",
                    userObject:"instagramUser object"}
            }
        };
        res.send(object);
    }

});

app.post('/api/activescreens', function (req,res) {
    var instagramName = req.body.userName;

    var instagramUserObjs = req.body.userObject;

    if(!!instagramName){
        instagramName = FireBase_Child_Check(instagramName);
        FireBase_Get_ActiveScreens(instagramName,function (awnser1) {
            res.send(awnser1);
        });

    }else if(!!instagramUserObjs && !!instagramUserObjs.username){
        instagramUserObjs.username = FireBase_Child_Check( instagramUserObjs.username);
        FireBase_Get_ActiveScreens(instagramUserObjs.username,function (awnser2) {
            res.send(awnser2);
        });
    }else{
        var object = {
            msg:"Wrong api Call, needs one of the expected object",
            status:401,
            data:{
                expectedobject: {userName:"instagramUsername",
                                 userObject:"instagramUser object"}
            }
        };
        res.send(object);
    }

});

app.get('/data/images',function(req,res){
   var tag = req.query.tag;
    var token = req.query.token;
    getJSON("https://api.instagram.com/v1/tags/"+tag+"/media/recent?access_token="+token+"&count=100",function(body){

        if (body!= null && body!= undefined){
            var images =[];
            var data = body.data;
            console.log(body);

            for(i=0;i<data.length;i++){
                images.push(data[i].images.standard_resolution.url);
                console.log('new image:'+ data[i].images.standard_resolution.url);
            }
            res.json(images);
        }

    });

});
app.get("/login/auth",function(req,res){
    var redirecturl = process.env.HOST + "/login/callback";
   res.redirect('https://api.instagram.com/oauth/authorize/?client_id='+process.env.client_id+'&redirect_uri='+redirecturl+'&response_type=code&scope=public_content');
});

app.get("/login/callback",function(req,res){
    var code = req.query.code;
    var error = req.query.error;
    if (!!code) {
        request.post("https://api.instagram.com/oauth/access_token", function optionalCallback(err, httpResponse, body) {
            data = JSON.parse(body);
            if(!!data.access_token) {
                res.redirect(process.env.HOST +"/#/?status=ok&token=" + data.access_token + "&user=" + JSON.stringify(data.user));
            }
            else {
                res.redirect(process.env.HOST +"/#/?status=err")
            }
            }).form({
            "client_id": process.env.client_id,
            "client_secret": process.env.client_secret,
            "grant_type": "authorization_code",
            "redirect_uri": process.env.HOST +"/login/callback",
            "code": code
        });
        console.log(code);
    }

});



app.listen(process.env.PORT || 3000);

function getJSON(url, callback) {
    request({
        url: url
    }, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            callback(JSON.parse(body));
        }else{
            callback(null);
            console.log(error+" " + JSON.stringify(response));
        }
    });
}

var FireBase_Child_Check = function (string) {
    var username = string;
    username = username.replace(".","_");
    username = username.replace("[","_");
    username = username.replace("]","_");
    username = username.replace("$","_");
    username = username.replace("#","_");

    return username;
};

var FireBase_Get_ActiveScreens = function (username,callback) {

    var myDataRef = new Firebase('https://tagshow.firebaseio.com/');
    var userRef = myDataRef.child("users");
    userRef.child(username).on("value",function(snapshot){
        console.log("success: "+snapshot.val());
        if(!snapshot.val()){
            var object = {
                msg:"Username wasn't found in the database",
                status:404,
                data:null
            };
            callback(object);
        }else{
            var firebaseUserRef = snapshot.val();
            var object = {
                msg:"call was succesful",
                status:200,
                data:{
                    ActiveScreens:firebaseUserRef.ActiveScreens
                }
            };
            callback(object);
        }
    },function(errorObject){
        console.log("error: "+errorObject);
        var object = {
            msg:"Database call failed. error code in data",
            status:500,
            data:errorObject
        };
        callback(object);
    });
};

var FireBase_Reset_ActiveScreens = function (username,callback) {

    var myDataRef = new Firebase('https://tagshow.firebaseio.com/');
    var userRef = myDataRef.child("users");
    userRef.child(username).update({"ActiveScreens":0},function(error){
        if(error){
            console.log("Reset ActiveScreens Error: "+error);
            var object = {
                msg:"Database call failed. error code in data",
                status:500,
                data:error
            };

            callback(object);
        }else{
            console.log("Reset ActiveScreens Success "+error);
            var object = {
                msg:"Database call Success.",
                status:200,
                data:error
            };

            callback(object);
        }

    });
};


