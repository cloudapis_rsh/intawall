var app = angular.module('tagshow', ['angular-storage', 'angular-jwt','ngRoute']);

app.controller("mainctrl",function($scope,$http,$interval,  store, $location, $rootScope, jwtHelper, $window,PaymentService){




    //slideshow varies
    $scope.model = {show_Payment : true};
    $scope.show_slideshow=true;
    $scope.show_home= true;
    $scope.slideshow_image_one = true;
    $scope.slideshow_image_two = false;
    $scope.slideshow_image_1 = "";
    $scope.slideshow_image_2 = "";
    $scope.slideshow_isplaying = false;
    $scope.tag = "newshoes";
    $scope.maxheight = $window.innerHeight;
    $scope.slideshow_show_mediabar = true;
    $scope.slideshow_show_ad = true;

    var payeduser = false;
    var images = [];



    //other varies
    var user = null;
    var firebaseUserRef;
    //var Website_address = "http://localhost:3000/";
    var Website_address = "http://tagshow.azurewebsites.net/";
    console.log("in controller");
    $scope.show_login_fields = false;
    $scope.show_signin_fields = false;
    $scope.mouse_out_of_fields = true;
    $scope.slideshow_show_overlay = false;

    var ad_init_done_once = false;
    var instagramtoken ="";

    //ad fix
    $(".mainPage").css({
        position: 'relative',
        zIndex: '1'
    });
    $(".ad").css({
        position: 'absolute',
        top:'0',
        left:'0',
        zIndex: '-10'
    });
    $(".images").css({
        position: 'relative',
        zIndex: '-5'
    }).css("background-color","white");

    var init = function(){
        $scope.slideshow_show_mediabar = false;
        $scope.model.show_Payment = false;
        if(ad_init_done_once == false){
            setTimeout(function(){
                console.log("started first delay");
                $scope.slideshow_show_ad = false;
                console.log($scope.slideshow_show_ad);
                $scope.show_slideshow = false;
                $(".images").css({
                    position: 'static',
                    zIndex: '1'
                });
                ad_init_done_once = true;
                console.log("ended ad fix");
            },3);
        }

        var url_vars = $location.search();
        console.log(JSON.stringify(url_vars));
        if (!!url_vars.token && !!url_vars.user ){
            $scope.logedin = true;
            user = JSON.parse(url_vars.user);
            $scope.name= user.username;
            instagramtoken = url_vars.token;
            store.set('user', url_vars.user);
            store.set('token', instagramtoken);
            $location.path("/home").search('token', null);
            $location.path("/home").search('user', null);
            $location.replace();
            console.log(url_vars.user);
            database_check_on_login();
        }else {
            $scope.logedin = false;
            $scope.name="";
        }
    };

    $rootScope.$on('exitPayment', function (event, args) {
        $scope.model.show_Payment = false;
        if(!!args){
            if(!!args.payment && !!args.customer){
                console.log("database update on payment done");
                var myDataRef = new Firebase('https://tagshow.firebaseio.com/');
                var userRef = myDataRef.child("users");
                username = user.username;
                username = username.replace(".","_");
                username =username.replace("[","_");
                username =username.replace("]","_");
                username =username.replace("$","_");
                username =username.replace("#","_");
                if(!!firebaseUserRef.stripe){
                    userRef.child(username+"/stripe/payments").push(args.payment);
                }else{
                    userRef.child(username+"/stripe").set({"customer_id":args.customer,"payments":[]});
                    userRef.child(username+"/stripe/payments").push(args.payment);
                }

                if (args.payment.amount <1000){
                    userRef.child(username).update({AccountType:1});
                }else if(args.payment.amount <2000){
                    userRef.child(username).update({AccountType:2});
                }else {
                    userRef.child(username).update({AccountType:3});
                }

            }
        }

    });
    
    $scope.mouse_exit = function(){
       if($scope.show_slideshow) {
           var res = confirm("If you leave this page the slideshow will close");
           if(res == true){
               $scope.slideshow_close();
           }
       }
    };

    window.onbeforeunload = function() {
        if($scope.show_slideshow) {
            $scope.slideshow_close();
            return ("The slideshow has quited!");
        }
    };
    
    $scope.Buy = function (am,plan) {
        PaymentService.setPaymentAmount(am);
        PaymentService.setPaymentPlan(plan);
        $rootScope.$emit('newPayment', user.username);
            $scope.model.show_Payment = true;
        console.log("pay your shit!");

    };

    $scope.clk_login = function(){
        $window.location.href = '/login/auth';
};

    $scope.clk_logout = function(){
        store.remove('profile');
        store.remove('token');
        $scope.logedin = false;
    };

    $scope.out_of_field_click = function(){
      if ($scope.mouse_out_of_fields && $scope.show_login_fields){
          $scope.show_login_fields = false;
      }
        if ($scope.mouse_out_of_fields && $scope.show_signin_fields){
            $scope.show_signin_fields = false;
        }
    };

    $scope.nav_Home_Click = function(){
        $scope.slideshow_close();

    };

    //Slideshow shit
    var pictureCounter = 0;
    var XnumberOverlay = 8;
    var DurationOverlay = 2;

    $scope.nav_Slideshow_Click = function(){
        $(".images").css("background-color","#222222");
        $(".ad").css({
             position: 'static',
             zIndex: '2',
             display: 'block',
             top: 'auto',
             left:'auto'
         });
        $scope.show_slideshow = true;
        $scope.show_home= false;

        firebaseUserRef.ActiveScreens ++;
        database_update_on_slideshow();

        switch(firebaseUserRef.AccountType){
            case 0:
                if( firebaseUserRef.ActiveScreens > 1){
                    alert("you ran out of screens! close existing slideshow or upgrade account");
                    $scope.slideshow_close();
                }
                break;
            case 1:
                if( firebaseUserRef.ActiveScreens > 2){
                    alert("you ran out of screens! close existing slideshow or upgrade account");
                    $scope.slideshow_close();
                }
                break;
            case 2:
                if( firebaseUserRef.ActiveScreens > 10){
                    alert("you ran out of screens! close existing slideshow or upgrade account");
                    $scope.slideshow_close();
                }
                break;
        }
        Get_new_images();

    };


    var image_nr = 2;

    $scope.Timer = null;

    //Timer start function.
    $scope.Start_Show = function () {

        $scope.Timer = $interval(function () {
            slideshow_next_image();

        }, 5000);
    };

    //Timer stop function.
    $scope.Stop_Show = function () {
        if (angular.isDefined($scope.Timer)) {
            $interval.cancel($scope.Timer);
        }
    };



    $scope.slideshow_close = function(){
        $scope.show_slideshow = false;
        $scope.show_home= true;
        $scope.slideshow_isplaying = false;
        $scope.Stop_Show();
        firebaseUserRef.ActiveScreens --;
        if(firebaseUserRef.ActiveScreens < 0){
            firebaseUserRef.ActiveScreens = 0;
        }
        database_update_on_slideshow();
    };

    $scope.slideshow_Play_Pause = function(){
        if($scope.slideshow_isplaying){
            $scope.slideshow_isplaying = false;
            $scope.Stop_Show();
        }else {
            $scope.slideshow_isplaying = true;
            $scope.Start_Show();
        }
    };

    $scope.slideshow_next = function(){
        slideshow_next_image();
        $scope.Stop_Show();
    };
    $scope.slideshow_prev = function(){
        slideshow_prev_image();
        $scope.Stop_Show();
    };

    var mediabar_hide_timer = null;

    var start_mediabar_hide_timer = function(){
        mediabar_hide_timer = $interval(function () {
            $scope.slideshow_show_mediabar = false;
            stop_mediabar_hide_timer();
        },2500);
    };

    var stop_mediabar_hide_timer = function(){
        if (angular.isDefined(mediabar_hide_timer)) {
            $interval.cancel(mediabar_hide_timer);
        }
    };

    $scope.slideshow_move = function(){
        console.log("slideshowmove clicked");
        $scope.slideshow_show_mediabar = true;
        stop_mediabar_hide_timer();
        start_mediabar_hide_timer();
    };



    var Get_new_images = function(){
        var tag = $scope.tag;

        //werkende instagram token voor alles omdat instagram sandbox shit
        instagramtoken = "3030103506.1677ed0.d1782660a09d477ab0a2a250a3768e2f";

        $http({
            method: 'GET',
            url: Website_address +'data/images?tag='+tag+'&token='+instagramtoken
        }).then(function successCallback(response) {
            images = response.data;
            $scope.maxheight = $window.innerHeight;
            $scope.slideshow_image_1 = images[0];
            $scope.slideshow_image_2 = images[1];
            image_nr = 2;

        }, function errorCallback(response) {
            console.log("failed: " + response);
        });
    };

    var lastimagetrue = "";
    var overlayStartNumber = 0;
    var overlayStopNumber = 0;
    var slideshow_next_image = function () {
        console.log(pictureCounter);
        if(pictureCounter%10==0 && !payeduser && pictureCounter!=0){
            console.log("started picturecounter% = 0 - " + pictureCounter);
            if(!$scope.slideshow_show_ad){
                console.log($scope.slideshow_show_ad);
                $scope.slideshow_show_overlay = false;
                if($scope.slideshow_image_one){
                    lastimagetrue = "one";
                    $scope.slideshow_image_one =false;
                }
                else if($scope.slideshow_image_two){
                    lastimagetrue = "two";
                    $scope.slideshow_image_two =false;
                }
                $scope.slideshow_show_ad = true;
                console.log($scope.slideshow_show_ad);
            }
            else{
                if(lastimagetrue == "one"){$scope.slideshow_image_one = true;}
                else if(lastimagetrue == "two"){$scope.slideshow_image_two = true;}
                $scope.slideshow_show_ad = false;
                console.log("ended slideshowad else if")
            }
        }
        if(!$scope.slideshow_show_ad){
            if(pictureCounter%XnumberOverlay == 0 && pictureCounter!=0){
                overlayStartNumber = pictureCounter;
                overlayStopNumber = pictureCounter + DurationOverlay-1;
                console.log("started overlay")
            }
            //Overlay with How to get your picture on
            //***NEEDS TO BE REWRITTEN***
            if(pictureCounter >= overlayStartNumber && pictureCounter <= overlayStopNumber){
                console.log(pictureCounter + " " + overlayStartNumber +" "+ overlayStopNumber);
                $scope.slideshow_show_overlay = true;
                /*if(pictureCounter == XnumberOverlay + DurationOverlay-1){
                    pictureCounter = 0;
                }*/
            }
            else {
                $scope.slideshow_show_overlay = false;
            }
            //***REWRITE STOP***

            if (image_nr >= images.length){
                image_nr = 0;
                Get_new_images();
            }
            if($scope.slideshow_image_one == true){
                $scope.slideshow_image_one = false;
                $scope.slideshow_image_two = true;
                $scope.slideshow_image_1 = images[image_nr];
            }else{
                $scope.slideshow_image_one = true;
                $scope.slideshow_image_two = false;
                $scope.slideshow_image_2 = images[image_nr];
            }
            image_nr ++;
            $scope.maxheight = $window.innerHeight;
            pictureCounter++;
        }
        //console.log(pictureCounter);

    };

    var slideshow_prev_image = function () {
        if (image_nr < 0){
            image_nr = images.length;
        }
        if($scope.slideshow_image_one == true){
            $scope.slideshow_image_one = false;
            $scope.slideshow_image_two = true;
            $scope.slideshow_image_1 = images[image_nr];
        }else{
            $scope.slideshow_image_one = true;
            $scope.slideshow_image_two = false;
            $scope.slideshow_image_2 = images[image_nr];
        }
        image_nr --;
        $scope.maxheight = $window.innerHeight;
    };

    var username;
    var database_check_on_login = function(){
        console.log("database check on login started");
        var myDataRef = new Firebase('https://tagshow.firebaseio.com/');
        var userRef = myDataRef.child("users");
        username = user.username;
        username = username.replace(".","_");
        username =username.replace("[","_");
        username =username.replace("]","_");
        username =username.replace("$","_");
        username =username.replace("#","_");
        userRef.child(username).on("value",function(snapshot){
            console.log("success: "+snapshot.val());
            if(!snapshot.val()){
                console.log("add new user to db");
                userRef.child(username).set({
                    insta_id: user.id,
                    full_name: user.full_name,
                    AccountType:0,
                    ActiveScreens:0
                });
                database_check_on_login();
            }else{
                firebaseUserRef = snapshot.val();
                console.log(username);

                userRef.child(username).on("child_changed", function(snapshot) {
                    
                });

                if(firebaseUserRef.AccountType < 1){
                    payeduser = false;
                }else {
                    payeduser = true;
                }

                if(firebaseUserRef.ActiveScreens <=0 && $scope.show_slideshow==true){
                    alert("All Slideshows has been stopped externally");
                    $scope.slideshow_close();
                }
                console.log(JSON.stringify(firebaseUserRef));
            }
        },function(errorObject){
            console.log("error: "+errorObject);
        });
        console.log("database check on login stopped");
    };

    var database_update_on_slideshow = function(){
        console.log("database update on slideshow started");
        var myDataRef = new Firebase('https://tagshow.firebaseio.com/');
        var userRef = myDataRef.child("users");
        username = user.username;
        username = username.replace(".","_");
        username =username.replace("[","_");
        username =username.replace("]","_");
        username =username.replace("$","_");
        username =username.replace("#","_");
        userRef.child(username).set(firebaseUserRef);
        console.log("database update on slideshow stopped");
    };


    $scope.Body_Style = function () {

        var style = {
            "height":  $window.innerHeight

        };

        return style

    };




    //todo: at the end
    init();

});

String.prototype.hashCode = function() {
    for(var ret = 0, i = 0, len = this.length; i < len; i++) {
        ret = (31 * ret + this.charCodeAt(i)) << 0;
    }
    return ret;
};


