/**
 * Created by Hans Van Staey on 25/04/2016.
 */

var app = angular.module("tagshow");
app.controller("payment", function ($scope, $http, $rootScope, $window, PaymentService) {

    var payment, customerId;

    //var Website_address = "http://localhost:3000/";
    var Website_address = "http://tagshow.azurewebsites.net/";
    var username;
    
    $rootScope.$on('newPayment', function (event, args) {
        $scope.show_payForm= true;
        var $form = $('#payment-form');
        $form.find('.submit').prop('disabled', false);
        $scope.price = PaymentService.PaymentAmount;
        $scope.plan = PaymentService.PaymentPlan;
        username = args;
        

        if ($scope.price == 0) {
            // free plan highlighted
            $scope.priceShow = "€ " + (($scope.price)/100).toString();
            //console.log($scope.priceShow);
        } else if($scope.price == 999) {
            // 2 screen plan highlighted
            $scope.priceShow = "€ " + (($scope.price)/100).toString();
            //console.log($scope.priceShow);
        } else if ($scope.price == 1999) {
            // 10 screen plan highlighted
            $scope.priceShow = "€ " + (($scope.price)/100).toString();
            //console.log($scope.priceShow);
        } else if ($scope.price == 2999) {
            // unlimited plan highlighted
            $scope.priceShow = "€ " + (($scope.price)/100).toString();
            //console.log($scope.priceShow);
        }
    });

        $scope.exitPayment = function (isok) {
                data = {};
                data.payment = payment;
                data.customer = customerId;
            
        
        $rootScope.$emit('exitPayment',data);
    };

    $scope.priceShow = (($scope.price)/100);

    $scope.show_payForm= true;

    $(function () {
        var $form = $('#payment-form');
        $form.submit(function (event) {
            // Disable the submit button to prevent repeated clicks:
            $form.find('.submit').prop('disabled', true);

            /*
             if (Stripe.card.validateCardNumber($('#number').val())
             && Stripe.card.validateExpiry($('#exp').val())
             && Stripe.card.validateCVC($('#cvc').val())) {

             // Request a token from Stripe:
             Stripe.card.createToken($form, stripeResponseHandler);
             } else {
             $form.find('.payment-errors').text('Please enter valid test credit card information.');
             $form.find('.submit').prop('disabled', false);
             }
             */

            // Request a token from Stripe:
            Stripe.card.createToken($form, stripeResponseHandler);

            // Prevent the form from being submitted:
            return false;
        });
    });

    function stripeResponseHandler(status, response) {
        // Grab the form:
        var $form = $('#payment-form');

        if (response.error) { // Problem!

            // Show the errors on the form:
            $form.find('.payment-errors').text(response.error.message);
            $form.find('.submit').prop('disabled', false); // Re-enable submission

        } else { // Token was created!

            // Get the token ID:
            var token = response.id;

            // Insert the token ID into the form so it gets submitted to the server:
            $form.append($('<input type="hidden" name="stripeToken">').val(token));


            console.log($form);

            //add price too form
            var body = {
                price: $scope.price,
                form: $form.serializeObject(),
                username: username
            };

            $http.post(Website_address+"pay", body)
                .success(function (data, status, headers, config) {
                    //console.log(data);
                    if (!!data) {
                        if (data.success) {
                            // overzicht betaling
                            $scope.show_payForm= false;

                            customerId = data.data.customer;

                            payment = {
                                id: data.data.id,
                                amount: data.data.amount,
                                balance_transaction: data.data.balance_transaction,
                                currency: data.data.currency,
                                failure_code: data.data.failure_code,
                                failure_message: data.data.failure_message
                            };

                            console.log(payment);
                            console.log(customerId);

                            $scope.exitPayment(true);

                        }
                        alert("Transaction successful, € " + ((data.data.amount)/100) + "\n" + $scope.plan);

                    }
                    //alert(JSON.stringify(data));
                }).error(function (data, status, headers, config) {
                    alert("failure message: " + JSON.stringify(data));
                }
            );
        }
    }
});

(function ($) {
    $.fn.serializeObject = function () {

        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push": /^$/,
                "fixed": /^\d+$/,
                "named": /^[a-zA-Z0-9_]+$/
            };


        this.build = function (base, key, value) {
            base[key] = value;
            return base;
        };

        this.push_counter = function (key) {
            if (push_counters[key] === undefined) {
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each($(this).serializeArray(), function () {

            // skip invalid keys
            if (!patterns.validate.test(this.name)) {
                return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while ((k = keys.pop()) !== undefined) {

                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // push
                if (k.match(patterns.push)) {
                    merge = self.build([], self.push_counter(reverse_key), merge);
                }

                // fixed
                else if (k.match(patterns.fixed)) {
                    merge = self.build([], k, merge);
                }

                // named
                else if (k.match(patterns.named)) {
                    merge = self.build({}, k, merge);
                }
            }

            json = $.extend(true, json, merge);
        });

        return json;
    };
})(jQuery);