/**
 * Created by rival on 28/04/2016.
 */
var app = angular.module("tagshow");
app.service('PaymentService', function() {
    this.setPaymentAmount= function (x) {
        this.PaymentAmount = x;
        //console.log('user changed: ' + JSON.stringify(this.User));
    };
    this.PaymentAmount = null;
    this.setPaymentPlan= function (x) {
        this.PaymentPlan = x;
        //console.log('user changed: ' + JSON.stringify(this.User));
    };
    this.PaymentPlan = null;
});